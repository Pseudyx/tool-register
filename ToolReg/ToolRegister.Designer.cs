﻿namespace ToolReg
{
    partial class ToolRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader Tool;
            System.Windows.Forms.ColumnHeader Contact;
            System.Windows.Forms.ColumnHeader DateTimeOut;
            this.gridColumnCalibrationEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnEditContactSearch = new DevExpress.XtraEditors.ButtonEdit();
            this.lblContactSearch = new DevExpress.XtraEditors.LabelControl();
            this.lblContact = new DevExpress.XtraEditors.LabelControl();
            this.lblContactName = new DevExpress.XtraEditors.LabelControl();
            this.lblToolSearch = new DevExpress.XtraEditors.LabelControl();
            this.btnEditToolSearch = new DevExpress.XtraEditors.ButtonEdit();
            this.xtraTab = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.grdToolOut = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnToolName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnToolDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLastCalibrated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnClearOut = new DevExpress.XtraEditors.SimpleButton();
            this.btnLbItemRemove = new DevExpress.XtraEditors.SimpleButton();
            this.lblNote = new DevExpress.XtraEditors.LabelControl();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.btnCheckOut = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.btnClearIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnCheckIn = new DevExpress.XtraEditors.SimpleButton();
            this.lblContactInName = new DevExpress.XtraEditors.LabelControl();
            this.lblContactIn = new DevExpress.XtraEditors.LabelControl();
            this.btnEditContactInSearch = new DevExpress.XtraEditors.ButtonEdit();
            this.lblContactInSearch = new DevExpress.XtraEditors.LabelControl();
            this.grdToolIn = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnTool = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDateOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblToolInDetail = new DevExpress.XtraEditors.LabelControl();
            this.btnEditToolInSearch = new DevExpress.XtraEditors.ButtonEdit();
            this.lblToolSearchIN = new DevExpress.XtraEditors.LabelControl();
            Tool = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            Contact = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            DateTimeOut = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.btnEditContactSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditToolSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTab)).BeginInit();
            this.xtraTab.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdToolOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditContactInSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdToolIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditToolInSearch.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridColumnCalibrationEnd
            // 
            this.gridColumnCalibrationEnd.Caption = "Calibration End";
            this.gridColumnCalibrationEnd.DisplayFormat.FormatString = "d";
            this.gridColumnCalibrationEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnCalibrationEnd.FieldName = "CalibrationEnd";
            this.gridColumnCalibrationEnd.Name = "gridColumnCalibrationEnd";
            this.gridColumnCalibrationEnd.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.gridColumnCalibrationEnd.Visible = true;
            this.gridColumnCalibrationEnd.VisibleIndex = 2;
            // 
            // btnEditContactSearch
            // 
            this.btnEditContactSearch.Location = new System.Drawing.Point(124, 38);
            this.btnEditContactSearch.Name = "btnEditContactSearch";
            this.btnEditContactSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnEditContactSearch.Properties.ValidateOnEnterKey = true;
            this.btnEditContactSearch.Size = new System.Drawing.Size(115, 20);
            this.btnEditContactSearch.TabIndex = 0;
            this.btnEditContactSearch.EditValueChanged += new System.EventHandler(this.btnEditContactSearch_EditValueChanged);
            this.btnEditContactSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEditContactSearch_KeyDown);
            this.btnEditContactSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnEditContactSearch_Click);
            // 
            // lblContactSearch
            // 
            this.lblContactSearch.Location = new System.Drawing.Point(43, 41);
            this.lblContactSearch.Name = "lblContactSearch";
            this.lblContactSearch.Size = new System.Drawing.Size(74, 13);
            this.lblContactSearch.TabIndex = 1;
            this.lblContactSearch.Text = "Contact Search";
            // 
            // lblContact
            // 
            this.lblContact.Location = new System.Drawing.Point(75, 71);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(42, 13);
            this.lblContact.TabIndex = 2;
            this.lblContact.Text = "Contact:";
            // 
            // lblContactName
            // 
            this.lblContactName.Location = new System.Drawing.Point(124, 71);
            this.lblContactName.Name = "lblContactName";
            this.lblContactName.Size = new System.Drawing.Size(0, 13);
            this.lblContactName.TabIndex = 3;
            // 
            // lblToolSearch
            // 
            this.lblToolSearch.Location = new System.Drawing.Point(61, 122);
            this.lblToolSearch.Name = "lblToolSearch";
            this.lblToolSearch.Size = new System.Drawing.Size(56, 13);
            this.lblToolSearch.TabIndex = 4;
            this.lblToolSearch.Text = "Tool Search";
            // 
            // btnEditToolSearch
            // 
            this.btnEditToolSearch.Location = new System.Drawing.Point(124, 119);
            this.btnEditToolSearch.Name = "btnEditToolSearch";
            this.btnEditToolSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnEditToolSearch.Properties.ValidateOnEnterKey = true;
            this.btnEditToolSearch.Size = new System.Drawing.Size(115, 20);
            this.btnEditToolSearch.TabIndex = 5;
            this.btnEditToolSearch.EditValueChanged += new System.EventHandler(this.btnEditToolSearch_EditValueChanged);
            this.btnEditToolSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEditToolSearch_KeyDown);
            this.btnEditToolSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnEditToolSearch_Click);
            // 
            // xtraTab
            // 
            this.xtraTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTab.Location = new System.Drawing.Point(0, 0);
            this.xtraTab.Name = "xtraTab";
            this.xtraTab.SelectedTabPage = this.xtraTabPage1;
            this.xtraTab.Size = new System.Drawing.Size(474, 510);
            this.xtraTab.TabIndex = 7;
            this.xtraTab.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Controls.Add(this.grdToolOut);
            this.xtraTabPage1.Controls.Add(this.btnClearOut);
            this.xtraTabPage1.Controls.Add(this.btnLbItemRemove);
            this.xtraTabPage1.Controls.Add(this.lblNote);
            this.xtraTabPage1.Controls.Add(this.memoNote);
            this.xtraTabPage1.Controls.Add(this.btnCheckOut);
            this.xtraTabPage1.Controls.Add(this.lblContactSearch);
            this.xtraTabPage1.Controls.Add(this.btnEditContactSearch);
            this.xtraTabPage1.Controls.Add(this.btnEditToolSearch);
            this.xtraTabPage1.Controls.Add(this.lblContact);
            this.xtraTabPage1.Controls.Add(this.lblToolSearch);
            this.xtraTabPage1.Controls.Add(this.lblContactName);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(468, 482);
            this.xtraTabPage1.Text = "Check Out";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(81, 145);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 13);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Details:";
            // 
            // grdToolOut
            // 
            this.grdToolOut.Location = new System.Drawing.Point(124, 145);
            this.grdToolOut.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.grdToolOut.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdToolOut.MainView = this.gridView2;
            this.grdToolOut.Name = "grdToolOut";
            this.grdToolOut.Size = new System.Drawing.Size(308, 200);
            this.grdToolOut.TabIndex = 12;
            this.grdToolOut.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.grdToolOut.Load += new System.EventHandler(this.grdToolOut_Load);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnToolName,
            this.gridColumnToolDesc,
            this.gridColumnCalibrationEnd,
            this.gridColumnLastCalibrated});
            this.gridView2.GridControl = this.grdToolOut;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnToolName
            // 
            this.gridColumnToolName.Caption = "Tool";
            this.gridColumnToolName.FieldName = "Name";
            this.gridColumnToolName.Name = "gridColumnToolName";
            this.gridColumnToolName.Visible = true;
            this.gridColumnToolName.VisibleIndex = 0;
            // 
            // gridColumnToolDesc
            // 
            this.gridColumnToolDesc.Caption = "Description";
            this.gridColumnToolDesc.FieldName = "Description";
            this.gridColumnToolDesc.Name = "gridColumnToolDesc";
            this.gridColumnToolDesc.Visible = true;
            this.gridColumnToolDesc.VisibleIndex = 1;
            // 
            // gridColumnLastCalibrated
            // 
            this.gridColumnLastCalibrated.Caption = "Last Calibrated";
            this.gridColumnLastCalibrated.DisplayFormat.FormatString = "d";
            this.gridColumnLastCalibrated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnLastCalibrated.FieldName = "LastCalibrated";
            this.gridColumnLastCalibrated.Name = "gridColumnLastCalibrated";
            this.gridColumnLastCalibrated.Visible = true;
            this.gridColumnLastCalibrated.VisibleIndex = 3;
            // 
            // btnClearOut
            // 
            this.btnClearOut.Location = new System.Drawing.Point(11, 449);
            this.btnClearOut.Name = "btnClearOut";
            this.btnClearOut.Size = new System.Drawing.Size(75, 23);
            this.btnClearOut.TabIndex = 11;
            this.btnClearOut.Text = "Clear";
            this.btnClearOut.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnLbItemRemove
            // 
            this.btnLbItemRemove.Location = new System.Drawing.Point(180, 275);
            this.btnLbItemRemove.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btnLbItemRemove.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLbItemRemove.Name = "btnLbItemRemove";
            this.btnLbItemRemove.Size = new System.Drawing.Size(59, 23);
            this.btnLbItemRemove.TabIndex = 10;
            this.btnLbItemRemove.Text = "Remove";
            this.btnLbItemRemove.Click += new System.EventHandler(this.btnLbItemRemove_Click);
            // 
            // lblNote
            // 
            this.lblNote.Location = new System.Drawing.Point(90, 353);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(27, 13);
            this.lblNote.TabIndex = 9;
            this.lblNote.Text = "Note:";
            // 
            // memoNote
            // 
            this.memoNote.Location = new System.Drawing.Point(124, 351);
            this.memoNote.Name = "memoNote";
            this.memoNote.Size = new System.Drawing.Size(308, 63);
            this.memoNote.TabIndex = 8;
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.Location = new System.Drawing.Point(384, 449);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(75, 23);
            this.btnCheckOut.TabIndex = 7;
            this.btnCheckOut.Text = "Check Out";
            this.btnCheckOut.Click += new System.EventHandler(this.btnCheckOut_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.btnClearIn);
            this.xtraTabPage2.Controls.Add(this.btnCheckIn);
            this.xtraTabPage2.Controls.Add(this.lblContactInName);
            this.xtraTabPage2.Controls.Add(this.lblContactIn);
            this.xtraTabPage2.Controls.Add(this.btnEditContactInSearch);
            this.xtraTabPage2.Controls.Add(this.lblContactInSearch);
            this.xtraTabPage2.Controls.Add(this.grdToolIn);
            this.xtraTabPage2.Controls.Add(this.lblToolInDetail);
            this.xtraTabPage2.Controls.Add(this.btnEditToolInSearch);
            this.xtraTabPage2.Controls.Add(this.lblToolSearchIN);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(468, 482);
            this.xtraTabPage2.Text = "Check In";
            // 
            // btnClearIn
            // 
            this.btnClearIn.Location = new System.Drawing.Point(11, 449);
            this.btnClearIn.Name = "btnClearIn";
            this.btnClearIn.Size = new System.Drawing.Size(75, 23);
            this.btnClearIn.TabIndex = 12;
            this.btnClearIn.Text = "Clear";
            this.btnClearIn.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.Location = new System.Drawing.Point(384, 449);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(75, 23);
            this.btnCheckIn.TabIndex = 8;
            this.btnCheckIn.Text = "Check In";
            this.btnCheckIn.Click += new System.EventHandler(this.btnCheckIn_Click);
            // 
            // lblContactInName
            // 
            this.lblContactInName.Location = new System.Drawing.Point(124, 71);
            this.lblContactInName.Name = "lblContactInName";
            this.lblContactInName.Size = new System.Drawing.Size(0, 13);
            this.lblContactInName.TabIndex = 7;
            // 
            // lblContactIn
            // 
            this.lblContactIn.Location = new System.Drawing.Point(75, 71);
            this.lblContactIn.Name = "lblContactIn";
            this.lblContactIn.Size = new System.Drawing.Size(42, 13);
            this.lblContactIn.TabIndex = 6;
            this.lblContactIn.Text = "Contact:";
            // 
            // btnEditContactInSearch
            // 
            this.btnEditContactInSearch.Location = new System.Drawing.Point(124, 38);
            this.btnEditContactInSearch.Name = "btnEditContactInSearch";
            this.btnEditContactInSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnEditContactInSearch.Size = new System.Drawing.Size(115, 20);
            this.btnEditContactInSearch.TabIndex = 5;
            this.btnEditContactInSearch.EditValueChanged += new System.EventHandler(this.btnEditContactInSearch_EditValueChanged);
            this.btnEditContactInSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEditContactInSearch_KeyDown);
            this.btnEditContactInSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnEditContactInSearch_Click);
            // 
            // lblContactInSearch
            // 
            this.lblContactInSearch.Location = new System.Drawing.Point(43, 41);
            this.lblContactInSearch.Name = "lblContactInSearch";
            this.lblContactInSearch.Size = new System.Drawing.Size(74, 13);
            this.lblContactInSearch.TabIndex = 4;
            this.lblContactInSearch.Text = "Contact Search";
            // 
            // grdToolIn
            // 
            this.grdToolIn.Location = new System.Drawing.Point(124, 145);
            this.grdToolIn.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.grdToolIn.LookAndFeel.UseDefaultLookAndFeel = false;
            this.grdToolIn.MainView = this.gridView1;
            this.grdToolIn.Name = "grdToolIn";
            this.grdToolIn.Size = new System.Drawing.Size(308, 200);
            this.grdToolIn.TabIndex = 3;
            this.grdToolIn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnTool,
            this.gridColumnContact,
            this.gridColumnDateOut});
            this.gridView1.GridControl = this.grdToolIn;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnTool
            // 
            this.gridColumnTool.Caption = "Tool";
            this.gridColumnTool.FieldName = "Tool.Name";
            this.gridColumnTool.Name = "gridColumnTool";
            this.gridColumnTool.Visible = true;
            this.gridColumnTool.VisibleIndex = 0;
            // 
            // gridColumnContact
            // 
            this.gridColumnContact.Caption = "Contact";
            this.gridColumnContact.FieldName = "Contact.FullName";
            this.gridColumnContact.Name = "gridColumnContact";
            this.gridColumnContact.Visible = true;
            this.gridColumnContact.VisibleIndex = 1;
            // 
            // gridColumnDateOut
            // 
            this.gridColumnDateOut.Caption = "Out";
            this.gridColumnDateOut.FieldName = "DateTimeOut";
            this.gridColumnDateOut.Name = "gridColumnDateOut";
            this.gridColumnDateOut.Visible = true;
            this.gridColumnDateOut.VisibleIndex = 2;
            // 
            // lblToolInDetail
            // 
            this.lblToolInDetail.Location = new System.Drawing.Point(81, 145);
            this.lblToolInDetail.Name = "lblToolInDetail";
            this.lblToolInDetail.Size = new System.Drawing.Size(36, 13);
            this.lblToolInDetail.TabIndex = 2;
            this.lblToolInDetail.Text = "Details:";
            // 
            // btnEditToolInSearch
            // 
            this.btnEditToolInSearch.Location = new System.Drawing.Point(124, 119);
            this.btnEditToolInSearch.Name = "btnEditToolInSearch";
            this.btnEditToolInSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnEditToolInSearch.Size = new System.Drawing.Size(115, 20);
            this.btnEditToolInSearch.TabIndex = 1;
            this.btnEditToolInSearch.EditValueChanged += new System.EventHandler(this.btnEditToolInSearch_EditValueChanged);
            this.btnEditToolInSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEditToolInSearch_KeyDown);
            this.btnEditToolInSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnEditToolInSearch_Click);
            // 
            // lblToolSearchIN
            // 
            this.lblToolSearchIN.Location = new System.Drawing.Point(61, 122);
            this.lblToolSearchIN.Name = "lblToolSearchIN";
            this.lblToolSearchIN.Size = new System.Drawing.Size(56, 13);
            this.lblToolSearchIN.TabIndex = 0;
            this.lblToolSearchIN.Text = "Tool Search";
            // 
            // ToolRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 507);
            this.Controls.Add(this.xtraTab);
            this.Name = "ToolRegister";
            this.Text = "ToolRegister";
            ((System.ComponentModel.ISupportInitialize)(this.btnEditContactSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditToolSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTab)).EndInit();
            this.xtraTab.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdToolOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditContactInSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdToolIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditToolInSearch.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ButtonEdit btnEditContactSearch;
        private DevExpress.XtraEditors.LabelControl lblContactSearch;
        private DevExpress.XtraEditors.LabelControl lblContact;
        private DevExpress.XtraEditors.LabelControl lblContactName;
        private DevExpress.XtraEditors.LabelControl lblToolSearch;
        private DevExpress.XtraEditors.ButtonEdit btnEditToolSearch;
        private DevExpress.XtraTab.XtraTabControl xtraTab;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton btnCheckOut;
        private DevExpress.XtraEditors.SimpleButton btnLbItemRemove;
        private DevExpress.XtraEditors.LabelControl lblNote;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraEditors.LabelControl lblToolInDetail;
        private DevExpress.XtraEditors.ButtonEdit btnEditToolInSearch;
        private DevExpress.XtraEditors.LabelControl lblToolSearchIN;
        private DevExpress.XtraGrid.GridControl grdToolIn;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTool;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContact;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDateOut;
        private DevExpress.XtraEditors.LabelControl lblContactIn;
        private DevExpress.XtraEditors.ButtonEdit btnEditContactInSearch;
        private DevExpress.XtraEditors.LabelControl lblContactInSearch;
        private DevExpress.XtraEditors.SimpleButton btnCheckIn;
        private DevExpress.XtraEditors.LabelControl lblContactInName;
        private DevExpress.XtraEditors.SimpleButton btnClearOut;
        private DevExpress.XtraEditors.SimpleButton btnClearIn;
        private DevExpress.XtraGrid.GridControl grdToolOut;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnToolName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnToolDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCalibrationEnd;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLastCalibrated;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}