﻿namespace ToolReg
{
    partial class ToolList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdTool = new DevExpress.XtraGrid.GridControl();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUniqueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCalibrationEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LastCalibrated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.ToolBar = new DevExpress.XtraBars.Bar();
            this.barBtnAddTool = new DevExpress.XtraBars.BarButtonItem();
            this.AddToolPopupControlContainer = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.btnToolCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnToolSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnToolClear = new DevExpress.XtraEditors.SimpleButton();
            this.lblToolID = new DevExpress.XtraEditors.LabelControl();
            this.txtToolID = new DevExpress.XtraEditors.TextEdit();
            this.lblCalExpire = new DevExpress.XtraEditors.LabelControl();
            this.dateCalExpire = new DevExpress.XtraEditors.DateEdit();
            this.memoToolDesc = new DevExpress.XtraEditors.MemoEdit();
            this.lblToolDesc = new DevExpress.XtraEditors.LabelControl();
            this.lblToolName = new DevExpress.XtraEditors.LabelControl();
            this.txtToolName = new DevExpress.XtraEditors.TextEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.grdTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddToolPopupControlContainer)).BeginInit();
            this.AddToolPopupControlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCalExpire.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCalExpire.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoToolDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // grdTool
            // 
            this.grdTool.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdTool.CausesValidation = false;
            this.grdTool.DataSource = this.bindingSource1;
            this.grdTool.Location = new System.Drawing.Point(12, 41);
            this.grdTool.MainView = this.gridView1;
            this.grdTool.Name = "grdTool";
            this.grdTool.Size = new System.Drawing.Size(448, 415);
            this.grdTool.TabIndex = 0;
            this.grdTool.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.grdTool.Load += new System.EventHandler(this.grdTool_Load);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "Tools";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName,
            this.gridColumnDescription,
            this.gridColumnUniqueID,
            this.gridColumnCalibrationEnd,
            this.LastCalibrated});
            this.gridView1.GridControl = this.grdTool;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "Name";
            this.gridColumnName.FieldName = "Name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            // 
            // gridColumnDescription
            // 
            this.gridColumnDescription.Caption = "Description";
            this.gridColumnDescription.FieldName = "Description";
            this.gridColumnDescription.Name = "gridColumnDescription";
            this.gridColumnDescription.Visible = true;
            this.gridColumnDescription.VisibleIndex = 1;
            // 
            // gridColumnUniqueID
            // 
            this.gridColumnUniqueID.Caption = "UniqueID";
            this.gridColumnUniqueID.FieldName = "UniqueID";
            this.gridColumnUniqueID.Name = "gridColumnUniqueID";
            this.gridColumnUniqueID.Visible = true;
            this.gridColumnUniqueID.VisibleIndex = 2;
            // 
            // gridColumnCalibrationEnd
            // 
            this.gridColumnCalibrationEnd.Caption = "Calibration Expire";
            this.gridColumnCalibrationEnd.DisplayFormat.FormatString = "d";
            this.gridColumnCalibrationEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnCalibrationEnd.FieldName = "CalibrationEnd";
            this.gridColumnCalibrationEnd.Name = "gridColumnCalibrationEnd";
            this.gridColumnCalibrationEnd.Visible = true;
            this.gridColumnCalibrationEnd.VisibleIndex = 3;
            // 
            // LastCalibrated
            // 
            this.LastCalibrated.Caption = "Last Calibrated";
            this.LastCalibrated.DisplayFormat.FormatString = "d";
            this.LastCalibrated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LastCalibrated.Name = "LastCalibrated";
            this.LastCalibrated.Visible = true;
            this.LastCalibrated.VisibleIndex = 4;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.ToolBar});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barBtnAddTool});
            this.barManager1.MainMenu = this.ToolBar;
            this.barManager1.MaxItemId = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.BarName = "Tool Menu";
            this.ToolBar.DockCol = 0;
            this.ToolBar.DockRow = 0;
            this.ToolBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ToolBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAddTool)});
            this.ToolBar.OptionsBar.AllowQuickCustomization = false;
            this.ToolBar.OptionsBar.DisableClose = true;
            this.ToolBar.OptionsBar.MultiLine = true;
            this.ToolBar.OptionsBar.UseWholeRow = true;
            this.ToolBar.Text = "Tool Menu";
            // 
            // barBtnAddTool
            // 
            this.barBtnAddTool.ActAsDropDown = true;
            this.barBtnAddTool.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barBtnAddTool.Caption = "Add Tool";
            this.barBtnAddTool.DropDownControl = this.AddToolPopupControlContainer;
            this.barBtnAddTool.Id = 0;
            this.barBtnAddTool.Name = "barBtnAddTool";
            // 
            // AddToolPopupControlContainer
            // 
            this.AddToolPopupControlContainer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.AddToolPopupControlContainer.Controls.Add(this.btnToolCancel);
            this.AddToolPopupControlContainer.Controls.Add(this.btnToolSave);
            this.AddToolPopupControlContainer.Controls.Add(this.btnToolClear);
            this.AddToolPopupControlContainer.Controls.Add(this.lblToolID);
            this.AddToolPopupControlContainer.Controls.Add(this.txtToolID);
            this.AddToolPopupControlContainer.Controls.Add(this.lblCalExpire);
            this.AddToolPopupControlContainer.Controls.Add(this.dateCalExpire);
            this.AddToolPopupControlContainer.Controls.Add(this.memoToolDesc);
            this.AddToolPopupControlContainer.Controls.Add(this.lblToolDesc);
            this.AddToolPopupControlContainer.Controls.Add(this.lblToolName);
            this.AddToolPopupControlContainer.Controls.Add(this.txtToolName);
            this.AddToolPopupControlContainer.Location = new System.Drawing.Point(12, 28);
            this.AddToolPopupControlContainer.Manager = this.barManager1;
            this.AddToolPopupControlContainer.Name = "AddToolPopupControlContainer";
            this.AddToolPopupControlContainer.Size = new System.Drawing.Size(315, 238);
            this.AddToolPopupControlContainer.TabIndex = 6;
            this.AddToolPopupControlContainer.Visible = false;
            // 
            // btnToolCancel
            // 
            this.btnToolCancel.Location = new System.Drawing.Point(95, 204);
            this.btnToolCancel.Name = "btnToolCancel";
            this.btnToolCancel.Size = new System.Drawing.Size(75, 23);
            this.btnToolCancel.TabIndex = 10;
            this.btnToolCancel.Text = "Cancel";
            this.btnToolCancel.Click += new System.EventHandler(this.btnToolCancel_Click);
            // 
            // btnToolSave
            // 
            this.btnToolSave.Location = new System.Drawing.Point(13, 204);
            this.btnToolSave.Name = "btnToolSave";
            this.btnToolSave.Size = new System.Drawing.Size(75, 23);
            this.btnToolSave.TabIndex = 9;
            this.btnToolSave.Text = "OK";
            this.btnToolSave.Click += new System.EventHandler(this.btnToolSave_Click);
            // 
            // btnToolClear
            // 
            this.btnToolClear.Location = new System.Drawing.Point(226, 204);
            this.btnToolClear.Name = "btnToolClear";
            this.btnToolClear.Size = new System.Drawing.Size(75, 23);
            this.btnToolClear.TabIndex = 8;
            this.btnToolClear.Text = "Clear";
            this.btnToolClear.Click += new System.EventHandler(this.btnToolClear_Click);
            // 
            // lblToolID
            // 
            this.lblToolID.Location = new System.Drawing.Point(13, 142);
            this.lblToolID.Name = "lblToolID";
            this.lblToolID.Size = new System.Drawing.Size(34, 13);
            this.lblToolID.TabIndex = 7;
            this.lblToolID.Text = "Tool ID";
            // 
            // txtToolID
            // 
            this.txtToolID.Location = new System.Drawing.Point(95, 139);
            this.txtToolID.MenuManager = this.barManager1;
            this.txtToolID.Name = "txtToolID";
            this.txtToolID.Size = new System.Drawing.Size(206, 20);
            this.txtToolID.TabIndex = 6;
            this.txtToolID.EditValueChanged += new System.EventHandler(this.txtToolID_EditValueChanged);
            // 
            // lblCalExpire
            // 
            this.lblCalExpire.Location = new System.Drawing.Point(13, 168);
            this.lblCalExpire.Name = "lblCalExpire";
            this.lblCalExpire.Size = new System.Drawing.Size(80, 13);
            this.lblCalExpire.TabIndex = 5;
            this.lblCalExpire.Text = "Calibraion Expire";
            // 
            // dateCalExpire
            // 
            this.dateCalExpire.EditValue = null;
            this.dateCalExpire.Location = new System.Drawing.Point(95, 165);
            this.dateCalExpire.MenuManager = this.barManager1;
            this.dateCalExpire.Name = "dateCalExpire";
            this.dateCalExpire.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCalExpire.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCalExpire.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateCalExpire.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateCalExpire.Size = new System.Drawing.Size(100, 20);
            this.dateCalExpire.TabIndex = 4;
            // 
            // memoToolDesc
            // 
            this.memoToolDesc.Location = new System.Drawing.Point(95, 36);
            this.memoToolDesc.MenuManager = this.barManager1;
            this.memoToolDesc.Name = "memoToolDesc";
            this.memoToolDesc.Size = new System.Drawing.Size(206, 96);
            this.memoToolDesc.TabIndex = 3;
            // 
            // lblToolDesc
            // 
            this.lblToolDesc.Location = new System.Drawing.Point(13, 36);
            this.lblToolDesc.Name = "lblToolDesc";
            this.lblToolDesc.Size = new System.Drawing.Size(76, 13);
            this.lblToolDesc.TabIndex = 2;
            this.lblToolDesc.Text = "Tool Description";
            // 
            // lblToolName
            // 
            this.lblToolName.Location = new System.Drawing.Point(13, 13);
            this.lblToolName.Name = "lblToolName";
            this.lblToolName.Size = new System.Drawing.Size(50, 13);
            this.lblToolName.TabIndex = 1;
            this.lblToolName.Text = "Tool Name";
            // 
            // txtToolName
            // 
            this.txtToolName.Location = new System.Drawing.Point(95, 10);
            this.txtToolName.MenuManager = this.barManager1;
            this.txtToolName.Name = "txtToolName";
            this.txtToolName.Size = new System.Drawing.Size(206, 20);
            this.txtToolName.TabIndex = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(472, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 468);
            this.barDockControlBottom.Size = new System.Drawing.Size(472, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 446);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(472, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 446);
            // 
            // ToolList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 468);
            this.Controls.Add(this.AddToolPopupControlContainer);
            this.Controls.Add(this.grdTool);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ToolList";
            this.Text = "Tool List";
            ((System.ComponentModel.ISupportInitialize)(this.grdTool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddToolPopupControlContainer)).EndInit();
            this.AddToolPopupControlContainer.ResumeLayout(false);
            this.AddToolPopupControlContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCalExpire.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCalExpire.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoToolDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdTool;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUniqueID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCalibrationEnd;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar ToolBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barBtnAddTool;
        private DevExpress.XtraBars.PopupControlContainer AddToolPopupControlContainer;
        private DevExpress.XtraEditors.LabelControl lblToolName;
        private DevExpress.XtraEditors.TextEdit txtToolName;
        private DevExpress.XtraEditors.LabelControl lblCalExpire;
        private DevExpress.XtraEditors.DateEdit dateCalExpire;
        private DevExpress.XtraEditors.MemoEdit memoToolDesc;
        private DevExpress.XtraEditors.LabelControl lblToolDesc;
        private DevExpress.XtraEditors.SimpleButton btnToolCancel;
        private DevExpress.XtraEditors.SimpleButton btnToolSave;
        private DevExpress.XtraEditors.SimpleButton btnToolClear;
        private DevExpress.XtraEditors.LabelControl lblToolID;
        private DevExpress.XtraEditors.TextEdit txtToolID;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn LastCalibrated;


    }
}