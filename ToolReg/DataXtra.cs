﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolReg
{
    public partial class Contact : System.Data.Objects.DataClasses.EntityObject
    {
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }
    }
}
