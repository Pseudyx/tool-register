﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Timers;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.ListControls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace ToolReg
{
    public partial class ToolRegister : Form
    {
        private ToolRegEntities ToolRegDB;
        private int OutContactID;
        private int InContactID;
        private List<Tool> ToolListOut;
        private List<Register> ToolListIN;

        public ToolRegister()
        {
            InitializeComponent();

            ToolRegDB = new ToolRegEntities();
            ToolListOut = new List<Tool>();
            ToolListIN = new List<Register>();

            btnEditContactSearch.Properties.EditValueChangedFiringMode = EditValueChangedFiringMode.Buffered;
            btnEditContactSearch.Properties.EditValueChangedDelay = 1000;
            btnEditToolSearch.Properties.EditValueChangedFiringMode = EditValueChangedFiringMode.Buffered;
            btnEditToolSearch.Properties.EditValueChangedDelay = 1000;

            btnEditContactInSearch.Properties.EditValueChangedFiringMode = EditValueChangedFiringMode.Buffered;
            btnEditContactInSearch.Properties.EditValueChangedDelay = 1000;
            btnEditToolInSearch.Properties.EditValueChangedFiringMode = EditValueChangedFiringMode.Buffered;
            btnEditToolInSearch.Properties.EditValueChangedDelay = 1000;
        }

        #region Check OUT

        private void btnEditContactSearch_Click(object sender, MouseEventArgs e)
        {
            ContactOutSearch();
        }

        private void btnEditContactSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ContactOutSearch();
            }
        }

        private void btnEditContactSearch_EditValueChanged(object sender, EventArgs e)
        {
            ContactOutSearch();
        }

        private void ContactOutSearch()
        {
            if (btnEditContactSearch.EditValue != null)
            {
                string BarCode = btnEditContactSearch.EditValue.ToString();
                var contact = ToolRegDB.Contacts.Where(x => x.UniqueID == BarCode).FirstOrDefault();
                if (contact != null)
                {
                    lblContactName.Text = contact.FirstName + " " + contact.LastName;
                    OutContactID = contact.ContactID;
                }

                btnEditContactSearch.EditValue = null;
            }  
        }

        private void btnEditToolSearch_Click(object sender, MouseEventArgs e)
        {
            ToolOutSearch();
        }

        private void btnEditToolSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ToolOutSearch();
            }
        }

        private void btnEditToolSearch_EditValueChanged(object sender, EventArgs e)
        {
            ToolOutSearch();
        }

        private void ToolOutSearch()
        {
            if (btnEditToolSearch.EditValue != null)
            {
                string BarCode = btnEditToolSearch.EditValue.ToString();
                var tool = ToolRegDB.Tools.Where(x => x.UniqueID == BarCode).FirstOrDefault();
                if (tool != null)
                {
                    if(ToolListOut.Contains(tool)){
                        btnEditToolSearch.EditValue = null;
                        return;
                    };

                    var reg = ToolRegDB.Registers.Where(x => x.ToolID == tool.ToolID && x.DateTimeIN == null).FirstOrDefault();
                    if (!(reg != null))
                    {
                        ToolListOut.Add(tool);
                        grdToolOut.DataSource = ToolListOut;
                        grdToolOut.RefreshDataSource();
                    }
                    else
                    {
                        MessageBox.Show(tool.Name + " is currenlt Checked Out to " + reg.Contact.FullName + " and must be checked in before being checked out again"
                            , "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                btnEditToolSearch.EditValue = null;
            }
        }

        private void btnLbItemRemove_Click(object sender, EventArgs e)
        {
            int[] selRows = ((GridView)grdToolOut.MainView).GetSelectedRows();
            DataRowView selRow = (DataRowView)(((GridView)grdToolOut.MainView).GetRow(selRows[0]));
            Tool tool = ToolListOut.Where(x=> x.Name == selRow["name"].ToString()).FirstOrDefault();

            ToolListOut.Remove(tool);

            grdToolOut.DataSource = ToolListOut;
            grdToolOut.RefreshDataSource();
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (OutContactID == 0)
            {
                MessageBox.Show("Please Select Contact Checking Out", "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            foreach (Tool tool in ToolListOut)
            {
                Register rg = ToolRegDB.Registers.CreateObject();
                rg.ContactID = OutContactID;
                rg.ToolID = tool.ToolID;
                rg.DateTimeOut = DateTime.Now;
                rg.Note = (memoNote.EditValue != null) ? memoNote.EditValue.ToString() : string.Empty;
                
                ToolRegDB.Registers.AddObject(rg);
            }

            ToolRegDB.SaveChanges();

            OutContactID = 0;
            lblContactName.Text = null;

            grdToolOut.DataSource = null;
            grdToolOut.RefreshDataSource();

            memoNote.EditValue = null;

            UpdateRegisterListChildren();
        }


        #endregion

        #region Check IN

        private void btnEditContactInSearch_Click(object sender, MouseEventArgs e)
        {
            ContactInSearch();
        }

        private void btnEditContactInSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ContactInSearch();
            }
        }

        private void btnEditContactInSearch_EditValueChanged(object sender, EventArgs e)
        {
            ContactInSearch();
        }

        private void ContactInSearch()
        {
            if (btnEditContactInSearch.EditValue != null)
            {
                string BarCode = btnEditContactInSearch.EditValue.ToString();
                var contact = ToolRegDB.Contacts.Where(x => x.UniqueID == BarCode).FirstOrDefault();
                if (contact != null)
                {
                    lblContactInName.Text = contact.FirstName + " " + contact.LastName;
                    InContactID = contact.ContactID;
                }

                btnEditContactInSearch.EditValue = null;
            }
        }

        private void btnEditToolInSearch_Click(object sender, MouseEventArgs e)
        {
            ToolInSearch();
        }

        private void btnEditToolInSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ToolInSearch();
            }
        }

        private void btnEditToolInSearch_EditValueChanged(object sender, EventArgs e)
        {
            ToolInSearch();
        }

        private void ToolInSearch()
        {
            if (btnEditToolInSearch.EditValue != null)
            {
                string BarCode = btnEditToolInSearch.EditValue.ToString();
                var tool = ToolRegDB.Tools.Where(t => t.UniqueID == BarCode).FirstOrDefault();
                var reg = ToolRegDB.Registers.Where(z => z.ToolID == tool.ToolID && z.DateTimeIN == null).FirstOrDefault();

                if (reg != null)
                {
                    if (ToolListIN.Contains(reg))
                    {
                        btnEditToolInSearch.EditValue = null;
                        return;
                    }

                    if (!ToolListIN.Contains(reg)) ToolListIN.Add(reg);
                    grdToolIn.DataSource = ToolListIN;
                    grdToolIn.RefreshDataSource();
                }
                else
                {
                    var last = ToolRegDB.Registers.Where(z => z.ToolID == tool.ToolID && z.DateTimeIN != null)
                                                  .OrderByDescending(x => x.DateTimeIN)
                                                  .Take(1)
                                                  .FirstOrDefault();
                    if (last != null)
                    {
                        MessageBox.Show(tool.Name + " is currently checked in, and was last checked in at: " + last.DateTimeIN.ToString() + " by " + last.CheckInContact.FullName
                                       , "Tool Register"
                                       , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        MessageBox.Show(tool.Name + " is currently checked in."
                                       , "Tool Register"
                                       , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                btnEditToolInSearch.EditValue = null;
            }
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            if (InContactID == 0)
            {
                MessageBox.Show("Please Select Contact Checking In", "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            foreach (Register rg in ToolListIN)
            {
                rg.DateTimeIN = DateTime.Now;
                rg.CheckInBy = InContactID;
            }
            ToolRegDB.SaveChanges();

            UpdateRegisterListChildren();

            InContactID = 0;
            lblContactInName.Text = null;
            ToolListIN.Clear();

            grdToolIn.DataSource = null;
            grdToolIn.RefreshDataSource();
        }


        #endregion

        #region Shared Function

        private void UpdateRegisterListChildren()
        {
            foreach (Form frm in MdiParent.MdiChildren)
            {
                if (frm.Name == "RegisterList")
                {
                    GridControl grd = (GridControl)frm.Controls.Find("grdRegister", true)[0];
                    grd.DataSource = null; 
                    grd.RefreshDataSource();

                    ToolRegDB = new ToolRegEntities();
                    var Reg = ToolRegDB.Registers.OrderByDescending(x => x.DateTimeOut);
                    ToolRegDB.Refresh(System.Data.Objects.RefreshMode.StoreWins, Reg);
                    grd.DataSource = Reg;
                    grd.RefreshDataSource();                    
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            OutContactID = 0;
            btnEditContactSearch.EditValue = null;
            lblContactName.Text = null;
            btnEditToolSearch.EditValue = null;
            ToolListOut.Clear();
            grdToolOut.DataSource = null;
            grdToolOut.RefreshDataSource();
            memoNote.EditValue = null;

            InContactID = 0;
            btnEditContactInSearch.EditValue = null;
            lblContactInName.Text = null;
            btnEditToolInSearch.EditValue = null;
            ToolListIN.Clear();
            grdToolIn.DataSource = null;
            grdToolIn.RefreshDataSource();
        }

        #endregion

        private void grdToolOut_Load(object sender, EventArgs e)
        {
            StyleFormatCondition cn1 = new StyleFormatCondition(FormatConditionEnum.Between, gridView2.Columns["CalibrationEnd"], null, DateTime.Now, DateTime.Now.AddDays(28));
            cn1.Appearance.BackColor = Color.Yellow;
            cn1.ApplyToRow = true;
            gridView2.FormatConditions.Add(cn1);

            StyleFormatCondition cn2 = new StyleFormatCondition(FormatConditionEnum.Between, gridView2.Columns["CalibrationEnd"], null, DateTime.Now, DateTime.Now.AddDays(7));
            cn2.Appearance.BackColor = Color.Red;
            cn2.Appearance.ForeColor = Color.White;
            cn2.ApplyToRow = true;
            gridView2.FormatConditions.Add(cn2);
        }

    }
}
