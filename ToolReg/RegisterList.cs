﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ToolReg
{
    public partial class RegisterList : Form
    {
        private ToolRegEntities ToolRegDB;

        public RegisterList()
        {
            InitializeComponent();

            ToolRegDB = new ToolRegEntities();
        }

        private void RegisterList_Load(object sender, EventArgs e)
        {
            grdRegister.DataSource = ToolRegDB.Registers.OrderByDescending(x=> x.DateTimeOut);
            grdRegister.RefreshDataSource();
        }
    }
}
