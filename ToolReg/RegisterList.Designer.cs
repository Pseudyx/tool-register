﻿namespace ToolReg
{
    partial class RegisterList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdRegister = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTool = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCheckInBy = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdRegister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // grdRegister
            // 
            this.grdRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdRegister.Location = new System.Drawing.Point(12, 12);
            this.grdRegister.MainView = this.gridView1;
            this.grdRegister.Name = "grdRegister";
            this.grdRegister.Size = new System.Drawing.Size(448, 483);
            this.grdRegister.TabIndex = 0;
            this.grdRegister.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnContact,
            this.gridColumnTool,
            this.gridColumnOut,
            this.gridColumnNote,
            this.gridColumnIN,
            this.gridColumnCheckInBy});
            this.gridView1.GridControl = this.grdRegister;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // gridColumnContact
            // 
            this.gridColumnContact.Caption = "Contact";
            this.gridColumnContact.FieldName = "Contact.FullName";
            this.gridColumnContact.Name = "gridColumnContact";
            this.gridColumnContact.Visible = true;
            this.gridColumnContact.VisibleIndex = 1;
            // 
            // gridColumnTool
            // 
            this.gridColumnTool.Caption = "Tool";
            this.gridColumnTool.FieldName = "Tool.Name";
            this.gridColumnTool.Name = "gridColumnTool";
            this.gridColumnTool.Visible = true;
            this.gridColumnTool.VisibleIndex = 0;
            // 
            // gridColumnOut
            // 
            this.gridColumnOut.Caption = "OUT";
            this.gridColumnOut.FieldName = "DateTimeOut";
            this.gridColumnOut.Name = "gridColumnOut";
            this.gridColumnOut.Visible = true;
            this.gridColumnOut.VisibleIndex = 2;
            // 
            // gridColumnNote
            // 
            this.gridColumnNote.Caption = "Note";
            this.gridColumnNote.FieldName = "Note";
            this.gridColumnNote.Name = "gridColumnNote";
            this.gridColumnNote.Visible = true;
            this.gridColumnNote.VisibleIndex = 4;
            // 
            // gridColumnIN
            // 
            this.gridColumnIN.Caption = "IN";
            this.gridColumnIN.FieldName = "DateTimeIN";
            this.gridColumnIN.Name = "gridColumnIN";
            this.gridColumnIN.Visible = true;
            this.gridColumnIN.VisibleIndex = 3;
            // 
            // gridColumnCheckInBy
            // 
            this.gridColumnCheckInBy.Caption = "Check In By";
            this.gridColumnCheckInBy.FieldName = "CheckInContact.FullName";
            this.gridColumnCheckInBy.Name = "gridColumnCheckInBy";
            this.gridColumnCheckInBy.Visible = true;
            this.gridColumnCheckInBy.VisibleIndex = 5;
            // 
            // RegisterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 507);
            this.Controls.Add(this.grdRegister);
            this.Name = "RegisterList";
            this.Text = "RegisterList";
            this.Load += new System.EventHandler(this.RegisterList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdRegister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdRegister;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTool;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContact;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOut;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNote;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCheckInBy;
    }
}