﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ToolReg
{
    public partial class MDIMain : Form
    {

        public MDIMain()
        {
            InitializeComponent();
        }

        public void MDIMain_Load(object sender, EventArgs e)
        {
            ToolRegister toolRegister = new ToolRegister();
            toolRegister.MdiParent = this;
            toolRegister.StartPosition = FormStartPosition.Manual;
            toolRegister.Location = new Point(0, 0);
            toolRegister.ClientSize = new Size((this.ClientRectangle.Width / 2)-20, (this.ClientRectangle.Height - 90));
            toolRegister.Show();

            RegisterList registerList = new RegisterList();
            registerList.MdiParent = this;
            registerList.StartPosition = FormStartPosition.Manual;
            registerList.Location = new Point(toolRegister.ClientSize.Width+10, 0);
            registerList.ClientSize = new Size((this.ClientRectangle.Width / 2)-10, (this.ClientRectangle.Height - 90));
            registerList.Show();
        }

        private void Open_ToolRegister(object sender, EventArgs e)
        {
            ToolRegister toolRegister = new ToolRegister();
            toolRegister.MdiParent = this;
            toolRegister.Show();
        }

        private void Open_RegisterList(object sender, EventArgs e)
        {
            RegisterList registerList = new RegisterList();
            registerList.MdiParent = this;
            registerList.Show();
        }

        private void Open_ToolList(object sender, EventArgs e)
        {
            ToolList toolList = new ToolList();
            toolList.MdiParent = this;
            toolList.Show();
        }

        private void Open_ContactList(object sender, EventArgs e)
        {
            ContactList contactList = new ContactList();
            contactList.MdiParent = this;
            contactList.Show();
        }

        private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void tileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form mdiChild in MdiChildren)
            {
                mdiChild.Close();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }      
        
    }
}
