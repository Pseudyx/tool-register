﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;

namespace ToolReg
{
    public partial class ToolList : Form
    {
        private ToolRegEntities ToolRegDB;
        private bool UniqueTool;

        public ToolList()
        {
            InitializeComponent();
            ToolRegDB = new ToolRegEntities();

            grdTool.DataSource = ToolRegDB.Tools;
        }


        private void btnToolSave_Click(object sender, EventArgs e)
        {

            if (!UniqueTool)
            {
                MessageBox.Show("A tool with the Barcode " + txtToolID.EditValue.ToString() + " already exists"
                            , "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Tool tool = ToolRegDB.Tools.CreateObject();

                tool.Name = txtToolName.EditValue.ToString();
                tool.Description = (memoToolDesc.EditValue != null) ? memoToolDesc.EditValue.ToString() : string.Empty;
                tool.UniqueID = txtToolID.EditValue.ToString();
                tool.CalibrationEnd = (DateTime)dateCalExpire.EditValue;

                ToolRegDB.Tools.AddObject(tool);
                ToolRegDB.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            }
            catch
            {

            }
            finally
            {

                txtToolName.EditValue = string.Empty;
                memoToolDesc.EditValue= string.Empty;
                txtToolID.EditValue = string.Empty;
                dateCalExpire.EditValue = null;

                AddToolPopupControlContainer.HidePopup();
            }

            grdTool.DataSource = new ToolRegEntities().Tools;
            grdTool.RefreshDataSource();
        }

        private void btnToolCancel_Click(object sender, EventArgs e)
        {
            txtToolName.EditValue = string.Empty;
            memoToolDesc.EditValue = string.Empty;
            txtToolID.EditValue = string.Empty;
            dateCalExpire.EditValue = null;

            AddToolPopupControlContainer.HidePopup();
        }

        private void btnToolClear_Click(object sender, EventArgs e)
        {
            txtToolName.EditValue = string.Empty;
            memoToolDesc.EditValue = string.Empty;
            txtToolID.EditValue = string.Empty;
            dateCalExpire.EditValue = null;
            txtToolID.BackColor = Color.White;
            txtToolID.ForeColor = Color.Black;
        }

        void ToolList_Disposed(object sender, EventArgs e)
        {
            if (AddToolPopupControlContainer.IsDisposed) return;
            if (AddToolPopupControlContainer.Parent != null) return;
            AddToolPopupControlContainer.Dispose();
        }

        private void grdTool_Load(object sender, EventArgs e)
        {
            StyleFormatCondition cn1 = new StyleFormatCondition(FormatConditionEnum.Between, gridView1.Columns["CalibrationEnd"], null, DateTime.Now, DateTime.Now.AddDays(28));
            cn1.Appearance.BackColor = Color.Yellow;
            cn1.ApplyToRow = true;
            gridView1.FormatConditions.Add(cn1);

            StyleFormatCondition cn2 = new StyleFormatCondition(FormatConditionEnum.Between, gridView1.Columns["CalibrationEnd"], null, DateTime.Now, DateTime.Now.AddDays(7));
            cn2.Appearance.BackColor = Color.Red;
            cn2.Appearance.ForeColor = Color.White;
            cn2.ApplyToRow = true;
            gridView1.FormatConditions.Add(cn2);
        }

        private void txtToolID_EditValueChanged(object sender, EventArgs e)
        {
            string BarCode = txtToolID.EditValue.ToString();
            var tool = ToolRegDB.Tools.Where(x => x.UniqueID == BarCode).FirstOrDefault();

            if (tool != null)
            {
                txtToolID.BackColor = Color.Red;
                txtToolID.ForeColor = Color.Black;
                UniqueTool = false;
            }
            else
            {
                txtToolID.BackColor = Color.Green;
                txtToolID.ForeColor = Color.White;
                UniqueTool = true;
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            Tool row = (Tool)e.Row;
            var tool = ToolRegDB.Tools.Where(x => x.ToolID == row.ToolID).FirstOrDefault();

            var dups = ToolRegDB.Tools.Where(x => x.UniqueID == row.UniqueID && x.ToolID != row.ToolID).ToList();

            if (dups.Count > 0)
            {
                MessageBox.Show("Can not save chages. A Tool with the Barcode " + row.UniqueID.ToString() + " already exists"
                            , "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            else
            {
                ToolRegDB.SaveChanges();
            }
        }

        

    }
}
