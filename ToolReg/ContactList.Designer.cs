﻿namespace ToolReg
{
    partial class ContactList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdContact = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUniqueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.ContactBar = new DevExpress.XtraBars.Bar();
            this.barBtnAddContact = new DevExpress.XtraBars.BarButtonItem();
            this.AddContactPopupControlContainer = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.btnContactClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnContactCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnContactSave = new DevExpress.XtraEditors.SimpleButton();
            this.lblContactID = new DevExpress.XtraEditors.LabelControl();
            this.txtContactID = new DevExpress.XtraEditors.TextEdit();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.lblLastName = new DevExpress.XtraEditors.LabelControl();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.lblFirstName = new DevExpress.XtraEditors.LabelControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.grdContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddContactPopupControlContainer)).BeginInit();
            this.AddContactPopupControlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // grdContact
            // 
            this.grdContact.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdContact.CausesValidation = false;
            this.grdContact.Location = new System.Drawing.Point(12, 41);
            this.grdContact.MainView = this.gridView1;
            this.grdContact.Name = "grdContact";
            this.grdContact.Size = new System.Drawing.Size(448, 415);
            this.grdContact.TabIndex = 0;
            this.grdContact.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnContactID,
            this.gridColumnFirstName,
            this.gridColumnLastName,
            this.gridColumnUniqueID});
            this.gridView1.GridControl = this.grdContact;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // gridColumnContactID
            // 
            this.gridColumnContactID.Caption = "ContactID";
            this.gridColumnContactID.FieldName = "ContactID";
            this.gridColumnContactID.Name = "gridColumnContactID";
            // 
            // gridColumnFirstName
            // 
            this.gridColumnFirstName.Caption = "First Name";
            this.gridColumnFirstName.FieldName = "FirstName";
            this.gridColumnFirstName.Name = "gridColumnFirstName";
            this.gridColumnFirstName.Visible = true;
            this.gridColumnFirstName.VisibleIndex = 0;
            // 
            // gridColumnLastName
            // 
            this.gridColumnLastName.Caption = "Last Name";
            this.gridColumnLastName.FieldName = "LastName";
            this.gridColumnLastName.Name = "gridColumnLastName";
            this.gridColumnLastName.Visible = true;
            this.gridColumnLastName.VisibleIndex = 1;
            // 
            // gridColumnUniqueID
            // 
            this.gridColumnUniqueID.Caption = "UniqueID";
            this.gridColumnUniqueID.FieldName = "UniqueID";
            this.gridColumnUniqueID.Name = "gridColumnUniqueID";
            this.gridColumnUniqueID.Visible = true;
            this.gridColumnUniqueID.VisibleIndex = 2;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.ContactBar});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barBtnAddContact});
            this.barManager1.MainMenu = this.ContactBar;
            this.barManager1.MaxItemId = 1;
            // 
            // ContactBar
            // 
            this.ContactBar.BarName = "Contact Menu";
            this.ContactBar.DockCol = 0;
            this.ContactBar.DockRow = 0;
            this.ContactBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ContactBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAddContact)});
            this.ContactBar.OptionsBar.AllowQuickCustomization = false;
            this.ContactBar.OptionsBar.MultiLine = true;
            this.ContactBar.OptionsBar.UseWholeRow = true;
            this.ContactBar.Text = "Contact Menu";
            // 
            // barBtnAddContact
            // 
            this.barBtnAddContact.ActAsDropDown = true;
            this.barBtnAddContact.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barBtnAddContact.Caption = "Add Contact";
            this.barBtnAddContact.DropDownControl = this.AddContactPopupControlContainer;
            this.barBtnAddContact.Id = 0;
            this.barBtnAddContact.Name = "barBtnAddContact";
            // 
            // AddContactPopupControlContainer
            // 
            this.AddContactPopupControlContainer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.AddContactPopupControlContainer.Controls.Add(this.btnContactClear);
            this.AddContactPopupControlContainer.Controls.Add(this.btnContactCancel);
            this.AddContactPopupControlContainer.Controls.Add(this.btnContactSave);
            this.AddContactPopupControlContainer.Controls.Add(this.lblContactID);
            this.AddContactPopupControlContainer.Controls.Add(this.txtContactID);
            this.AddContactPopupControlContainer.Controls.Add(this.txtLastName);
            this.AddContactPopupControlContainer.Controls.Add(this.lblLastName);
            this.AddContactPopupControlContainer.Controls.Add(this.txtFirstName);
            this.AddContactPopupControlContainer.Controls.Add(this.lblFirstName);
            this.AddContactPopupControlContainer.Location = new System.Drawing.Point(12, 28);
            this.AddContactPopupControlContainer.Manager = this.barManager1;
            this.AddContactPopupControlContainer.Name = "AddContactPopupControlContainer";
            this.AddContactPopupControlContainer.Size = new System.Drawing.Size(315, 141);
            this.AddContactPopupControlContainer.TabIndex = 5;
            this.AddContactPopupControlContainer.Visible = false;
            // 
            // btnContactClear
            // 
            this.btnContactClear.Location = new System.Drawing.Point(225, 105);
            this.btnContactClear.Name = "btnContactClear";
            this.btnContactClear.Size = new System.Drawing.Size(75, 23);
            this.btnContactClear.TabIndex = 8;
            this.btnContactClear.Text = "Clear";
            this.btnContactClear.Click += new System.EventHandler(this.btnContactClear_Click);
            // 
            // btnContactCancel
            // 
            this.btnContactCancel.Location = new System.Drawing.Point(95, 105);
            this.btnContactCancel.Name = "btnContactCancel";
            this.btnContactCancel.Size = new System.Drawing.Size(75, 23);
            this.btnContactCancel.TabIndex = 7;
            this.btnContactCancel.Text = "Cancel";
            this.btnContactCancel.Click += new System.EventHandler(this.btnContactCancel_Click);
            // 
            // btnContactSave
            // 
            this.btnContactSave.Location = new System.Drawing.Point(13, 105);
            this.btnContactSave.Name = "btnContactSave";
            this.btnContactSave.Size = new System.Drawing.Size(75, 23);
            this.btnContactSave.TabIndex = 6;
            this.btnContactSave.Text = "OK";
            this.btnContactSave.Click += new System.EventHandler(this.btnContactSave_Click);
            // 
            // lblContactID
            // 
            this.lblContactID.Location = new System.Drawing.Point(13, 67);
            this.lblContactID.Name = "lblContactID";
            this.lblContactID.Size = new System.Drawing.Size(52, 13);
            this.lblContactID.TabIndex = 5;
            this.lblContactID.Text = "Contact ID";
            // 
            // txtContactID
            // 
            this.txtContactID.Location = new System.Drawing.Point(95, 64);
            this.txtContactID.MenuManager = this.barManager1;
            this.txtContactID.Name = "txtContactID";
            this.txtContactID.Size = new System.Drawing.Size(206, 20);
            this.txtContactID.TabIndex = 4;
            this.txtContactID.EditValueChanged += new System.EventHandler(this.txtContactID_EditValueChanged);
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(95, 37);
            this.txtLastName.MenuManager = this.barManager1;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(206, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // lblLastName
            // 
            this.lblLastName.Location = new System.Drawing.Point(13, 40);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(50, 13);
            this.lblLastName.TabIndex = 2;
            this.lblLastName.Text = "Last Name";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(95, 10);
            this.txtFirstName.MenuManager = this.barManager1;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(206, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Location = new System.Drawing.Point(13, 13);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(51, 13);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "First Name";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(472, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 468);
            this.barDockControlBottom.Size = new System.Drawing.Size(472, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 446);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(472, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 446);
            // 
            // ContactList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 468);
            this.Controls.Add(this.AddContactPopupControlContainer);
            this.Controls.Add(this.grdContact);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ContactList";
            this.Text = "Contact List";
            ((System.ComponentModel.ISupportInitialize)(this.grdContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddContactPopupControlContainer)).EndInit();
            this.AddContactPopupControlContainer.ResumeLayout(false);
            this.AddContactPopupControlContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdContact;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContactID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLastName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUniqueID;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar ContactBar;
        private DevExpress.XtraBars.BarButtonItem barBtnAddContact;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupControlContainer AddContactPopupControlContainer;
        private DevExpress.XtraEditors.LabelControl lblFirstName;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraEditors.LabelControl lblLastName;
        private DevExpress.XtraEditors.LabelControl lblContactID;
        private DevExpress.XtraEditors.TextEdit txtContactID;
        private DevExpress.XtraEditors.SimpleButton btnContactSave;
        private DevExpress.XtraEditors.SimpleButton btnContactCancel;
        private DevExpress.XtraEditors.SimpleButton btnContactClear;

    }
}