﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;

namespace ToolReg
{
    public partial class ContactList : Form
    {
        private ToolRegEntities ToolRegDB;
        private bool UniqueContact;

        public ContactList()
        {
            InitializeComponent();
            ToolRegDB = new ToolRegEntities();

            grdContact.DataSource = ToolRegDB.Contacts;
        }

        private void btnContactSave_Click(object sender, EventArgs e)
        {

            if (!UniqueContact)
            {
                MessageBox.Show("A Contact with the Barcode " + txtContactID.EditValue.ToString() + " already exists"
                            , "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Contact contact = ToolRegDB.Contacts.CreateObject();

                contact.FirstName = txtFirstName.EditValue.ToString();
                contact.LastName = txtLastName.EditValue.ToString();
                contact.UniqueID = txtContactID.EditValue.ToString();

                ToolRegDB.Contacts.AddObject(contact);
                ToolRegDB.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            }
            catch
            {

            }
            finally
            {

                txtFirstName.EditValue = string.Empty;
                txtLastName.EditValue = string.Empty;
                txtContactID.EditValue = string.Empty;

                AddContactPopupControlContainer.HidePopup();
            }

            grdContact.DataSource = new ToolRegEntities().Contacts;
            grdContact.RefreshDataSource();
        }

        private void btnContactCancel_Click(object sender, EventArgs e)
        {
            txtFirstName.EditValue = string.Empty;
            txtLastName.EditValue = string.Empty;
            txtContactID.EditValue = string.Empty;

            AddContactPopupControlContainer.HidePopup();
        }

        private void btnContactClear_Click(object sender, EventArgs e)
        {
            txtFirstName.EditValue = string.Empty;
            txtLastName.EditValue = string.Empty;
            txtContactID.EditValue = string.Empty;
            txtContactID.BackColor = Color.White;
            txtContactID.ForeColor = Color.Black;
        }

        void ToolList_Displosed(object sender, EventArgs e)
        {
            if (AddContactPopupControlContainer.IsDisposed) return;
            if (AddContactPopupControlContainer.Parent != null) return;
            AddContactPopupControlContainer.Dispose();
        }

        private void txtContactID_EditValueChanged(object sender, EventArgs e)
        {
            string BarCode = txtContactID.EditValue.ToString();
            var tool = ToolRegDB.Contacts.Where(x => x.UniqueID == BarCode).FirstOrDefault();

            if (tool != null)
            {
                txtContactID.BackColor = Color.Red;
                txtContactID.ForeColor = Color.Black;
                UniqueContact = false;
            }
            else
            {
                txtContactID.BackColor = Color.Green;
                txtContactID.ForeColor = Color.White;
                UniqueContact = true;
            }
        }

        private void gridView1_RowUpdated(object sender, RowObjectEventArgs e)
        {
            Contact row = (Contact)e.Row;
            var contact = ToolRegDB.Contacts.Where(x => x.ContactID == row.ContactID).FirstOrDefault();

            var dups = ToolRegDB.Contacts.Where(x => x.UniqueID == row.UniqueID && x.ContactID != row.ContactID).ToList();

            if (dups.Count > 0)
            {
                MessageBox.Show("Can not save chages. A Contact with the Barcode " + row.UniqueID.ToString() + " already exists"
                            , "Tool Register", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            else
            {
                ToolRegDB.SaveChanges();
            }

        }
    }
}
